
# Part 1: Forming A Nation And A Mosaic Population

## Chapter 1: From British Intervention to Independence

The British first controlled Penang in 1786, increasing their influence as the Dutch receded, with influence being progressively extended after the signign of the Pangkor Engagement in 1874. By WW1, Malay rulers of all the peninsula's states had accepted British protection. Since then the Malay peninsula was under the British sphere of influence until the Japanese occupation in 1941. 

Each of the peninsula's nine Malay states was rules by a monarch; 'Sultan' replaced 'Raja' with Islamisation. In Negeri Sembilan, the monarch is titled 'Yang fi-Pertuan Besar' (literally, 'he who is made lord'). Before colonial rule, the sultans would rule, but not govern. The royal court officials comprised a Bendahara (prime minister); a Bendahari (Treasurer); a Temenggong (Security chief); and a Mantri (Secretary). In the west coast states, district chiefs appointed by the sultans (nominally) provided executive leadership with responsibility for revenue collection, administration, defence, and justice (Gullick, 2004). Values, ceremonial traditions,, hereditary titles, and ranks played a part in maintaining the prevailing social order (Milner, 1982). 

Collectively, they were referred to as British Malaya, comprising of (i) the Straits Settlements: Penang, Melaka, and Singapore; (ii) the Federated Malay States: Selangor, Perak, Negeri Sembilan, and Pahang; and (iii) the Unfederated Malay States: Johor, Perlis, Kedah, Kelantan, and Terrenganu. The Straits Settlements were administered directly as a crown colony, whereas the Federated and Unfederated Malay States were protected Malay states. 

Economic migrants were drawn to the expansion of commercial agriculture, mainly in rubber, and the volatile tin industry. Colonial policy ensured that British Malaya was socially and economically divided; beteween the Malays, Chinese, and Indians (as categorised by the British); or between the English-educated elite class and rural class. 

The Japanese occupation weakened British colonial dominance, and after their surrender in August 1945, there was a period of military administration until March 1946. The British then tried to impose a centralised Malayan Union, but with vehement resistance, the Federation of Malaya was instead created on 1 Febrary 1948. This was a transition period to independence in August 1957. 

Sabah and Sarawak both became British-protected states in 1888, and British colonies in 1946. In September 1963, they merged with the Federation of Malaya, and Singapore, to form Malaysia.

### The Straits Settements of Penang, Melaka, and SIngapore

From 1825 to 1867, the Straits Settlements of Penang, Melaka, and Singapore were ruled from British India under the authority of the East India Company. In 1867, after a decade of petitioning by the the European merchant class, the British Crown Colony of the Straits Settlements was created and administered by the Colonial Office. This included Christmas island (1900), the Cocos Keeling islands (1903), and the Dindings Territory (from 1826 until its retrocession to Perak in 1935).

Each of the three Straits Settlements has free port status, which meant that shipping and cargo could enter and leave without taxes being levied. 

#### Penang

Penang was a centre for trading in opium from India to China; in various goods from Have; in rice from Siam; and in imports like cotton and arms from Great Britain. In the 1760s, the East India Company took notice of the trade in the Malay archipelago. The Dutch East India Company denied access to trade to the East India Company here. 

Francis Light, a shipmaster from the Company who has considerable dealings with Kedah, negotiated with Sultan Abdullah to cede Penang. Sultan Abdullah agreed to sign a treaty doiing so on the condition that the Company would come to his aid if Kedah was attacked militarily, but the Company did not agree to this condition. Instead, on 15 July 1786, Light landed in penang with 'troops, stores and guns', and took over Penang by 11 August 1786, renaming it Prince of Wales Island. The Sultan failed to drive the British out of Penang and was forced to sign a treaty ceding it in 1790.

In 1800, Perai on the peninsula, came under the jurisdiction of the East India Company and was renamed Province Wellesley. It supplied Penang with commodities for export, ensured control of Penang harbour, and served as a food supply. It grew rice and sugarcane, cultivated by the Malays and European enterprise respectively, employing Chinese workers.

\\should include map 1.1 for the aesthetic

### Melaka

In 1511, the Portugese conquered Melaka from its sultanate. In 1641, with the support of the Johor sultanate, the Dutch drove the portugese out of Melaka and made it their headquarters in the peninsula. In 1795, the East India Company took control of Melaka from the Dutch during the Napoleonic wars. It was handed back to the Dutch in 1818 with the Treaty of Vienna, but became a British posession again in 1824 with the Anglo-Ducth Treaty which divided the Malay archipelago into British and Dutch spheres. In exchange for Melaka, the British gave Bencoolen in West Sumatra to the Dutch. 

Melaka was producing tapioca and rice, and had open-cast tin mining. Melaka became commercially and strategically unimportant with the rise of Penang and Singapore. 

### Singapore

In 1819, Stamford Raffles signed a treaty on behalf of the East India Company with Sultan Hussein and Temenggong Abdul Rahman of Johor. In 1824, the British were given the right to settle on the island. 

SIngapore was a major destination for Chinese, Indian, and British goods. Singapore's revenues significantly exceeded Penang and Melaka's. With the construction of Keppel Harbour in the 1860s and the Suez Canal in 1869, SIngapore's trade was boosted. It exported tin and rubber in the late 19th and early 20th centuries. 

#### Governance of the Straits Settlements

In 1832, Singapore replaced Penang as the administrative centre of the Straits Settlements. After separating from the East India Company in 1867, a British governor was appointed, based in Singapore, while Penang and Melaka had lieutenant governors. The governor had executive powers, could appoint judges, and direct policies. There was an Advisory Executive Council, which included the resident councillors of Penang and Melaka and unofficial members from local communities. 

A Legislative Council that could make laws was formed in 1867, and in 1887 a Municipal Council was establsihed to administer town areas separately from rural districts. This model of colonial governance was later adopted in the Federated Malay States. Other departments included the treasury (1855); land office (1855); public works (1859); supreme court (1868); Chinese protectorate (1877); and Indian imigration department (1879).

### The Pangkor Engagement, 1874

In Perak, Selangor, and Negeri Sembilan, which produced tin, labour was attracted in the 1860s. Tin mines were owned by wealthy Chinese businessmen from the Straits Settlements, on land owned by Malay chiefs. In the early 1870s, violent disputes broke out over control of the mines between rival Chinese factions based on clan membership and dialect groups. 

In July 1842, a petition from 248 Chinese traders arrived in London to deliver news that the west coast, from Province Wellesley to Melaka, was in a state of anarchy. This in part prompted the Earl of Kimberley, then Colonial Secretary, to intervene. The elite in the Straits Settlements trading community, formed a Straits Settlements Association, and petitioned the British government to change its long-standing policy of 'non-interference in the Malay states'. 

Meanwhile, tehre was a succession dispute to the Perak throne following the death of Sultan Ali in May 1871. Power struggles and violent conflict were common in royal succession, especially for Malay clans to contest the rights to collect revenues from the tin-mining concessions. The rival Chinese clans backed the competing claimants to the Perak throne, and the Governor Harry Ord failed to resolve the dispute, or persuade the Colonial Office to intervene.

The Earl of Kimberly, the then British Colonial Secretary, appointed Andrew Clarke to be Governor of the Straits Settlements. In 1873, Raja Muda Abdullah of Perak (a claimant) requested that Andrew Clarke prepare a treaty with the British government. He invited the colonial administration to 'pacify' Perak and appoint a Resident. This treaty was signed on 20 January 1874. 

\\ There is an exceprt here with the Pangkor Engagement in full

The Residential system gave the British Resident control over all aspects of the state's adminsitration, except for matters relating to religion and customs, which were left to the sultan and religious scholars. Residents were instructed to focus on peace and security and to maintain a taxation system to pay for the British officers.

The Pangkor Engagement resoluved the dispites in the mines and the succession crisis. However, since it undermined traditional Malay institutions, in that revenue collection was centralised and local chiefs were disempowered and no longer allowed to collect and spend revenue as they pleased. This prevented the ability of other members of Malay royalty to accumulate wealth. The firts resident of Perak, James W. W. Birch, was murdered in 2 November 1875. Political resistance to the Engagement was eventually crushed.

Article nine of the Pangkor Engagement, "That a Civil List regulating the income to be received by the Sultan, by the Bandahara, by the Mantri, and by the other Officers be the next in charge of said Revenue", allowed the local chiefs to be compensated for this loss. It was only implemented in 1887, after the third Resident of Perak, Hugh Low, was appointed. 

This model was exported to other Malay states with similar civil wars over the rights to tin-mining royalties, sucj as in Selangor and Negeri Sembilan. The Malay rulers accepted a British-appointed Resident in exchange for security assurances and the maintainence of their sovreignty and rule. By curtailing powerful local chiefs, the sultans' positions were strengthened, and could accumulate prestige and wealth through the small shares they held in their states' revenue collection. 

The British ensures that critical aspetcs of traditional local power structure and culture were untouched, by leaving the issues of religion and custom to the sultan. The Residents were in turn supported by the agency of the Malay rulers, who held authority over their subjects. The Malays derived their sense of identity and position in life from the raja, and service to him was seen as the gateway to social and spiritual advancement (Milner, 1982). 

### The Federated Malay States and Their Governance